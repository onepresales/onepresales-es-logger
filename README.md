# Onepresales Elastic Search Logger module

This is a NPM module based on Winston (https://github.com/winstonjs/winston) and Winston ElasticSearch transport (https://github.com/vanthome/winston-elasticsearch).

IMPORTANT NOTE: **Winston ElasticSearch** transport it will be only used if ```process.env.NODE_ENV``` is defined. Otherwise only the **Console** transport is used.

## How to use it


``` javascript

	const indexPrefix = "qmi";
	const logger = require('onepresales-es-logger')("QMI");

	var logData = {
		"page":"packer/building-the-image.md",
		"qlikID":"a08D000000YKSOJIA5",
		"accountid":"220012000000JumuY",
		"email":"johndoe@unknown.com"
	}
	logger.info("viewpage", logData);

```

The document in Elastic Search will be inserted, by default, in the index **onepresales-log** and shape like the following

``` json

	{
		"_index":"onepresales-log",
		"_type":"log",
		"_id":"3mPUHmIBS2oXHFJHAWTT",
		"_score":1.0,
		"_source":{
			"tool": "QMI",
			"@timestamp":"2018-03-13T10:07:11.046Z",
			"message":"viewpage",
			"level":"info",
			"page":"packer/building-the-image.md",
			"qlikID":"a08D000000YKSOJIA5",
			"accountid":"220012000000JumuY",
			"email":"johndoe@unknown.com"
		}
	}

```