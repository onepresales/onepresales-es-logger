const winston = require('winston');
const ElasticSearchWinston = require('winston-elasticsearch');

function _init(toolName='common', indexName='onepresales-log') {
	var logger;
	var wConfig = {};

	if (process.env.NODE_ENV) {
		const esConfig = {
			node: process.env.ELASTICSEARCH_HOSTNAME_PORT || 'http://localhost:9200',
			auth: {
				username: process.env.ELASTICSEARCH_AUTH_USER || "user",
				password: process.env.ELASTICSEARCH_AUTH_PASSWORD || "ZCGVBk7Yl3JA"
			}
		};
		console.log("Creating Logger Winston ES for tool: " + toolName + " and Index: " + indexName);

		console.log("Logger ElasticSearch config :", esConfig);

		const { Client } = require('@elastic/elasticsearch');

		const esClient = new Client(esConfig);

		var esTransportOpts = {
			level: 'info',
			index: indexName,
			client: esClient,
			transformer: function(logData) {
				const transformed = {};
				transformed.tool = toolName;
				transformed['@timestamp'] = logData.timestamp ? logData.timestamp : new Date().toISOString();
				transformed.message = logData.message;
				transformed.level = logData.level;
				//transformed.fields = logData.meta;
				for (let key in logData.meta) {
					transformed[key] = logData.meta[key];
				}
				return transformed;
			}
		};
		wConfig.transports = [new ElasticSearchWinston(esTransportOpts)];
		
	} else {
		wConfig.transports = [
			new(winston.transports.Console)({
				timestamp: function() {
					return new Date().toJSON()
				},
				prettyPrint: true,
				json: true
			})
		];
	}

	logger = new(winston.Logger)(wConfig);
	console.log("Logger " + toolName + " created!");
	return logger;
}

module.exports = _init;